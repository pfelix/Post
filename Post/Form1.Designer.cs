﻿namespace Post
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxTitulo = new System.Windows.Forms.TextBox();
            this.LabelTitulo = new System.Windows.Forms.Label();
            this.LabelDescricao = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.ButtonPost = new System.Windows.Forms.Button();
            this.LabelData = new System.Windows.Forms.Label();
            this.ButtonGosto = new System.Windows.Forms.Button();
            this.ButtonNaoGosto = new System.Windows.Forms.Button();
            this.LabelErro = new System.Windows.Forms.Label();
            this.LabelGosto = new System.Windows.Forms.Label();
            this.LabelNãoGosto = new System.Windows.Forms.Label();
            this.ButtonNovoPost = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxTitulo
            // 
            this.TextBoxTitulo.Location = new System.Drawing.Point(49, 40);
            this.TextBoxTitulo.Name = "TextBoxTitulo";
            this.TextBoxTitulo.Size = new System.Drawing.Size(237, 20);
            this.TextBoxTitulo.TabIndex = 0;
            // 
            // LabelTitulo
            // 
            this.LabelTitulo.AutoSize = true;
            this.LabelTitulo.Location = new System.Drawing.Point(13, 44);
            this.LabelTitulo.Name = "LabelTitulo";
            this.LabelTitulo.Size = new System.Drawing.Size(35, 13);
            this.LabelTitulo.TabIndex = 1;
            this.LabelTitulo.Text = "Título";
            // 
            // LabelDescricao
            // 
            this.LabelDescricao.AutoSize = true;
            this.LabelDescricao.Location = new System.Drawing.Point(13, 75);
            this.LabelDescricao.Name = "LabelDescricao";
            this.LabelDescricao.Size = new System.Drawing.Size(55, 13);
            this.LabelDescricao.TabIndex = 2;
            this.LabelDescricao.Text = "Descrição";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(16, 91);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(351, 138);
            this.TextBoxDescricao.TabIndex = 3;
            // 
            // ButtonPost
            // 
            this.ButtonPost.Location = new System.Drawing.Point(268, 235);
            this.ButtonPost.Name = "ButtonPost";
            this.ButtonPost.Size = new System.Drawing.Size(90, 23);
            this.ButtonPost.TabIndex = 4;
            this.ButtonPost.Text = "Post";
            this.ButtonPost.UseVisualStyleBackColor = true;
            this.ButtonPost.Click += new System.EventHandler(this.ButtonPost_Click);
            // 
            // LabelData
            // 
            this.LabelData.AutoSize = true;
            this.LabelData.Location = new System.Drawing.Point(293, 44);
            this.LabelData.Name = "LabelData";
            this.LabelData.Size = new System.Drawing.Size(65, 13);
            this.LabelData.TabIndex = 5;
            this.LabelData.Text = "dd-mm-aaaa";
            // 
            // ButtonGosto
            // 
            this.ButtonGosto.Enabled = false;
            this.ButtonGosto.Location = new System.Drawing.Point(28, 235);
            this.ButtonGosto.Name = "ButtonGosto";
            this.ButtonGosto.Size = new System.Drawing.Size(95, 23);
            this.ButtonGosto.TabIndex = 6;
            this.ButtonGosto.Text = "Gosto";
            this.ButtonGosto.UseVisualStyleBackColor = true;
            this.ButtonGosto.Click += new System.EventHandler(this.ButtonGosto_Click);
            // 
            // ButtonNaoGosto
            // 
            this.ButtonNaoGosto.Enabled = false;
            this.ButtonNaoGosto.Location = new System.Drawing.Point(28, 262);
            this.ButtonNaoGosto.Name = "ButtonNaoGosto";
            this.ButtonNaoGosto.Size = new System.Drawing.Size(95, 23);
            this.ButtonNaoGosto.TabIndex = 7;
            this.ButtonNaoGosto.Text = "Não gosto";
            this.ButtonNaoGosto.UseVisualStyleBackColor = true;
            this.ButtonNaoGosto.Click += new System.EventHandler(this.ButtonNaoGosto_Click);
            // 
            // LabelErro
            // 
            this.LabelErro.AutoSize = true;
            this.LabelErro.BackColor = System.Drawing.Color.White;
            this.LabelErro.Location = new System.Drawing.Point(22, 206);
            this.LabelErro.Name = "LabelErro";
            this.LabelErro.Size = new System.Drawing.Size(26, 13);
            this.LabelErro.TabIndex = 8;
            this.LabelErro.Text = "Erro";
            this.LabelErro.Visible = false;
            // 
            // LabelGosto
            // 
            this.LabelGosto.AutoSize = true;
            this.LabelGosto.Location = new System.Drawing.Point(130, 240);
            this.LabelGosto.Name = "LabelGosto";
            this.LabelGosto.Size = new System.Drawing.Size(13, 13);
            this.LabelGosto.TabIndex = 9;
            this.LabelGosto.Text = "0";
            // 
            // LabelNãoGosto
            // 
            this.LabelNãoGosto.AutoSize = true;
            this.LabelNãoGosto.Location = new System.Drawing.Point(130, 267);
            this.LabelNãoGosto.Name = "LabelNãoGosto";
            this.LabelNãoGosto.Size = new System.Drawing.Size(13, 13);
            this.LabelNãoGosto.TabIndex = 10;
            this.LabelNãoGosto.Text = "0";
            // 
            // ButtonNovoPost
            // 
            this.ButtonNovoPost.Location = new System.Drawing.Point(16, 9);
            this.ButtonNovoPost.Name = "ButtonNovoPost";
            this.ButtonNovoPost.Size = new System.Drawing.Size(89, 23);
            this.ButtonNovoPost.TabIndex = 11;
            this.ButtonNovoPost.Text = "Novo Post";
            this.ButtonNovoPost.UseVisualStyleBackColor = true;
            this.ButtonNovoPost.Click += new System.EventHandler(this.ButtonNovoPost_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 289);
            this.Controls.Add(this.ButtonNovoPost);
            this.Controls.Add(this.LabelNãoGosto);
            this.Controls.Add(this.LabelGosto);
            this.Controls.Add(this.LabelErro);
            this.Controls.Add(this.ButtonNaoGosto);
            this.Controls.Add(this.ButtonGosto);
            this.Controls.Add(this.LabelData);
            this.Controls.Add(this.ButtonPost);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.LabelDescricao);
            this.Controls.Add(this.LabelTitulo);
            this.Controls.Add(this.TextBoxTitulo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxTitulo;
        private System.Windows.Forms.Label LabelTitulo;
        private System.Windows.Forms.Label LabelDescricao;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.Button ButtonPost;
        private System.Windows.Forms.Label LabelData;
        private System.Windows.Forms.Button ButtonGosto;
        private System.Windows.Forms.Button ButtonNaoGosto;
        private System.Windows.Forms.Label LabelErro;
        private System.Windows.Forms.Label LabelGosto;
        private System.Windows.Forms.Label LabelNãoGosto;
        private System.Windows.Forms.Button ButtonNovoPost;
    }
}

