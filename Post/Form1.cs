﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Post
{
    public partial class Form1 : Form
    {

        Post post;

        public Form1()
        {
            InitializeComponent();

            post = new Post();

            LabelData.Text = post.Data.ToString("dd-MM-yyyy");
        }


        private void ButtonPost_Click(object sender, EventArgs e)
        {
            if (TextBoxTitulo.Text == "")
            {
                LabelErro.Visible = true;
                LabelErro.Text = "Erro: Falta introduzir um título.";
            }
            else if (TextBoxDescricao.Text == "" )
            {
                LabelErro.Visible = true;
                LabelErro.Text = "Erro: Falta introduzir uma descrição.";
            }
            else
            {
                ButtonPost.Enabled = false;
                ButtonGosto.Enabled = true;
                ButtonNaoGosto.Enabled = true;
                LabelErro.Visible = false;
                TextBoxDescricao.Enabled = false;
                TextBoxTitulo.Enabled = false;
            }
            
        }

        private void ButtonGosto_Click(object sender, EventArgs e)
        {
            post.ContarGosto(1);
            LabelGosto.Text = post.Gosto.ToString();
        }
        private void ButtonNaoGosto_Click(object sender, EventArgs e)
        {
            post.ContarNaoGosto(1);
            LabelNãoGosto.Text = post.NaoGosto.ToString();
        }

        private void ButtonNovoPost_Click(object sender, EventArgs e)
        {
            post = new Post();

            LabelData.Text = post.Data.ToString("dd-MM-yyyy");
            LabelGosto.Text = post.Gosto.ToString();
            LabelNãoGosto.Text = post.NaoGosto.ToString();
            TextBoxTitulo.Text = post.Titulo;
            TextBoxDescricao.Text = post.Descricao;
            ButtonPost.Enabled = true;
            ButtonGosto.Enabled = false;
            ButtonNaoGosto.Enabled = false;
            LabelErro.Visible = false;
            TextBoxDescricao.Enabled = true;
            TextBoxTitulo.Enabled = true;
        }

    }
}
