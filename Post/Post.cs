﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Post
{
    public class Post
    {
        #region Atributos

        private string _titulo;
        private string _descricao;
        private readonly DateTime _data;
        private readonly int _gosto;
        private readonly int _naogosto;

        #endregion

        #region Propriedades

        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; private set; }
        public int Gosto { get; private set; }
        public int NaoGosto { get; private set; }

        #endregion

        #region Construtores
        //Default
        public Post ()
        {
            Titulo = "";
            Descricao = "";
            Data = DateTime.Today;
            Gosto = 0;
            NaoGosto = 0;
        }

        //Parametros
        public Post (string titulo, string descricao)
        {
            Titulo = titulo;
            Descricao = descricao;
            Data = DateTime.Today;
            Gosto = 0;
            NaoGosto = 0;
        }

        // Cópia
        public Post (Post post)
        {
            Titulo = post.Titulo;
            Descricao = post.Descricao;
            Data = post.Data;
            Gosto = post.Gosto;
            NaoGosto = post.NaoGosto;
        }

        #endregion

        #region Outros metodos

        public int ContarGosto(int valor)
        {
            Gosto += valor;

            return Gosto;
        }

        public int ContarNaoGosto (int valor)
        {
            NaoGosto -= valor;

            return NaoGosto;
        }
        #endregion

        #region Metodos gerais

        #endregion


    }
}
